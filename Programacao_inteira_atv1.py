import sys
import matplotlib.pyplot as plt
import shapely
import random
import numpy as np
from scipy.spatial import KDTree
from shapely.geometry import LineString, Point
 
# Drawing Shapes

def line_int(Pi, Pf, Pref):
    P = (0.0,Pref[1])
    line1 = LineString([Pi, Pf])
    line2 = LineString([Pref, P])
    int_pt = line1.intersection(line2)
    if int_pt:
        return True
    return False


def PlotFigure(P1, P2, P3, P4, P5):
    plt.axes() 
    points = [P1, P2, P3, P4, P5]
    line = plt.Polygon(points, closed=1, fill=None, edgecolor='r')
    #plt.plot(P_test[0],P_test[1],'b.')
    plt.gca().add_patch(line)
    plt.axis('scaled')


def is_vertice(Pi, Pf, Pref):
    yi, yf , yref = Pi[1], Pf[1], Pref[1]
    
    if (yref == yi or yref ==yf):
        return True
    return False
         
            
def Plot_Point(P1, P2, P3, P4, P5, P_test):
    count = 0;
    global n_point_;
    if ((P_test[1] == P3[1] and P_test[0] > P3[0]) or 
        (P_test[1] == P5[1] and P_test[0] > P5[0]) or 
        (P_test[1] == P2[1] and P_test[0] > P2[0]) or
        (P_test[1] == P1[1] and P_test[0] > P1[0])):
        count = count+1;
    if (line_int(P1, P2, P_test)): 
        count = count+1;  
    if (line_int(P2, P3, P_test)): 
        count = count+1;
    if (line_int(P3, P4, P_test)):
        count = count+1;
    if (line_int(P4, P5, P_test)):
        count = count+1;
    if (line_int(P5, P1, P_test)): 
        count = count+1;  
    if count > 0:
        #if(is_vertice):
        #    count = count-1;
        if (count % 2 != 0): 
            string_plt = 'b.'
            n_point_ = n_point_+1
        else:
            string_plt = 'g.'
    else:
            string_plt = 'g.'
    plt.plot(P_test[0],P_test[1],string_plt)


def Get_random_points():
    pts = np.random.random((1,2)) 
    return pts


if __name__ == '__main__':
    try:
        P1 = (0.1, 0.1)
        P2 = (0.8, 0.1)
        P3 = (0.6, 0.7)
        P4 = (0.5, 0.8)
        P5 = (0.3, 0.5)
        global n_point_
        max_n_point = 10000
        n_point_ = 0
        PlotFigure(P1, P2, P3, P4, P5)
        for i in range(0, max_n_point):
            point_test = Get_random_points()
            P_test = (round(point_test[0][0],4), round(point_test[0][1],4))
            Plot_Point(P1, P2, P3, P4, P5, P_test)
        print('Points Inside: '+ str(n_point_))
        area = n_point_/max_n_point
        print('Estimated Area: '+ str(area))
        plt.xlim(0,1)
        plt.ylim(0,1)
        plt.show()
    except KeyboardInterrupt:
        print("\nKeyboardInterrupt")
    sys.exit(0)

